# Spry Rails gem

The Spry Rails gem contains common functionality for Rails apps.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'spry', '~> 1.2.5', git: 'https://bitbucket.org/sprydk/spry-rails.git'
```

And then execute:

```
$ bundle install
```

---

## JSONAPI

When the gem is included in a Rails project the `application/vnd.api+json` is
automatically registered together with `application/json` for JSON responses.

### Handling exceptions

By added the following to your base API controller all exceptions will be returned
as JSON instead of HTML.

```ruby
include Spry::RescueExceptionsWithJson
```

The following exceptions that are handled are (in order):

```ruby
ActiveRecord::RecordNotFound
ActionController::ParameterMissing
Exception
```

### API responses

The following methods for API responses are available.

#### Success responses

| HTTP status    | Method name                                                                           |
| -------------- | ------------------------------------------------------------------------------------- |
| 200 Ok         | `Spry::JsonApi.new(self).ok(data: data)`                                              |
| 201 Created    | `Spry::JsonApi.new(self).created` or<br>`Spry::JsonApi.new(self).created(data: data)` |
| 202 Accepted   | `Spry::JsonApi.new(self).accepted`                                                    |
| 204 No content | `Spry::JsonApi.new(self).no_content`                                                  |

#### Redirects

| HTTP status           | Method name                                                   |
| --------------------- | ------------------------------------------------------------- |
| 301 Moved permanently | `Spry::JsonApi.new(self).moved_permanently(location: '/new')` |
| 302 Found             | `Spry::JsonApi.new(self).found(location: '/new')`             |
| 303 See other         | `Spry::JsonApi.new(self).see_other(location: '/new')`         |

#### Error responses

| HTTP status               | Method name                                                |
| ------------------------- | ---------------------------------------------------------- |
| 400 Bad request           | `Spry::JsonApi.new(self).bad_request`                      |
| 401 Unauthorized          | `Spry::JsonApi.new(self).unauthorized`                     |
| 403 Forbidden             | `Spry::JsonApi.new(self).forbidden`                        |
| 404 Not found             | `Spry::JsonApi.new(self).not_found`                        |
| 409 Conflict              | `Spry::JsonApi.new(self).conflict`                         |
| 422 Unprocessable entity  | `Spry::JsonApi.new(self).unprocessable_entity(errors: [])` |
| 500 Internal server error | `Spry::JsonApi.new(self).internal_server_error`            |

All error responses also takes optional `title` and `code` parameters,
fx: `Spry::JsonApi.new(self).forbidden(title: 'You need custom rights for this.', code: 'custom_forbidden_code')`.

The Spry::JsonApi.new(self).unprocessable_entity takes an optional `errors` parameter.
This can be either an array of strings or a `ActiveModel::Errors` instance in which case the
`full_messages` are returned.

---

## Bugsnag integration

The gem is integrated with Bugsnag to track errors.

Errors are forwarrded if the `BUGSNAG_KEY` env variable is set and the Rails app is running in `production` mode.

It is possible to also send errors when in `development` mode by setting the `BUGSNAG_SEND_DEVELOPMENT_ERRORS` to `true`.

Note that `ActiveRecord::RecordNotFound` and `ActionController::ParameterMissing` aren't forwarded.

---

## Development

After checking out the repo, run `bin/setup` to install dependencies. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Releasing a new version

- Bump version number in lib/spry/version.rb and in README under "Installation"
- Commit changes & push changes.
- git tag -a v<version_number>
- git push origin master
- git push --tags origin master

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
