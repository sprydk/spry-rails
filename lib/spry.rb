require 'rails'
require 'json'
require 'bugsnag'

require 'spry/version'
require 'spry/railtie'
require 'spry/json_api'
require 'spry/rescue_exceptions_with_json'

module Spry
  def self.root
    File.expand_path('../..', __FILE__)
  end
end
