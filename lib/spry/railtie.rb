require 'rails'

module Spry
  class Railtie < Rails::Railtie
    initializer "spry.configure" do
      
      # Configure mimetype for JSONAPI
      api_mime_types = %W(
        application/vnd.api+json
        application/json
      )
      Mime::Type.register 'application/vnd.api+json', :json, api_mime_types
      
      # Bugsnag setup
      if ENV.fetch('BUGSNAG_KEY', false) && (Rails.env.production? || ENV.fetch('BUGSNAG_SEND_DEVELOPMENT_ERRORS', false))
        Bugsnag.configure do |config|
          config.api_key = ENV.fetch('BUGSNAG_KEY')
        end
      end
      
    end
  end
end
