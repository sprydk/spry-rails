module Spry
  class JsonApi

    attr_reader :controller

    def initialize(controller)
      @controller = controller
    end

    ##########
    # Success:
    ##########

    # 200: Ok
    def ok(data: nil, status: '200')
      render_data(data: data, status: status)
    end

    # 201: Created
    def created(data: nil)
      render_data(data: data, status: '201')
    end

    # 202: Accepted
    def accepted
      controller.head('202')
    end

    # 204: No content
    def no_content
      controller.head('204')
    end

    #############
    # Redirection
    #############

    # 301: Moved permanently
    def moved_permanently(location: nil)
      render_redirect(location: location, status: '301')
    end

    # 302: Found
    def found(location: nil)
      render_redirect(location: location, status: '302')
    end

    # 303: See other
    def see_other(location: nil)
      render_redirect(location: location, status: '303')
    end

    ##############
    # Client error
    ##############

    # 400: Bad request
    def bad_request(title: 'Bad request', code: 'bad_request')
      render_error(title: title, code: code, status: '400')
    end

    # 401: Unauthorized
    def unauthorized(title: 'Unauthorized', code: 'unauthorized')
      render_error(title: title, code: code, status: '401')
    end

    # 403: Forbidden
    def forbidden(title: 'Forbidden', code: 'forbidden')
      render_error(title: title, code: code, status: '403')
    end

    # 404: Not found
    def not_found(title: 'Not found', code: 'not_found')
      render_error(title: title, code: code, status: '404')
    end

    # 409: Conflict
    def conflict(title: 'Conflict', code: 'conflict')
      render_error(title: title, code: code, status: '409')
    end

    # 422: Unprocessable entity
    def unprocessable_entity(title: 'Unprocessable entity', code: 'unprocessable_entity', errors: [])
      render_error(title: title, code: code, status: '422', errors: errors)
    end

    ##############
    # Server error
    ##############

    # 500: Internal server error
    def internal_server_error(title: 'Internal server error', code: 'internal_server_error')
      render_error(title: title, code: code, status: '500')
    end

    private

      def render_data(data: nil, status: nil)
        if data
          render_json(data: data, status: status)
        elsif status
          controller.head(status)
        else
          controller.head(:no_content)
        end
      end

      def render_redirect(location: nil, status: nil)
        data = {
          redirect: {
            location: location,
          }
        }
        render_data(data: data, status: status)
      end

      def render_error(title: nil, code: nil, status: nil, errors: [])
        formatted_errors = []

        if errors.size > 0
          if errors.instance_of?(ActiveModel::Errors)
            errors.full_messages.each do |error|
              formatted_errors << format_single_error(title: title, code: code, status: status, error: error)
            end
          else
            errors.each do |error|
              formatted_errors << format_single_error(title: title, code: code, status: status, error: error)
            end
          end
        else
          formatted_errors << format_single_error(title: title, code: code, status: status)
        end

        data = {
          errors: formatted_errors,
        }
        render_data(data: data, status: status)
      end

      def format_single_error(title: nil, code: nil, status: :bad_request, error: nil)
        data = {
          code: code,
          status: status,
          title: title,
        }
        data[:details] = error if error
        data
      end

      def render_json(data: nil, status: nil)
        @controller.render(json: data, status: status)
      end
      
  end
end
