module Spry
  module RescueExceptionsWithJson

    extend ActiveSupport::Concern
    include ActiveSupport::Rescuable

    included do

      # 'rescue_from' is evaluated bottom-to-top so we rescue general exception last.
      rescue_from Exception do |exception|
        Spry::JsonApi.new(self).internal_server_error(title: exception.message)
        Bugsnag.notify(exception)
      end

      rescue_from ActionController::ParameterMissing do |exception|
        Spry::JsonApi.new(self).bad_request(title: exception.message)
      end

      rescue_from ActiveRecord::RecordNotFound do |exception|
        Spry::JsonApi.new(self).not_found(title: exception.message)
      end

    end

  end
end
